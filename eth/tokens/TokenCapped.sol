// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Capped.sol";

contract DevToken is ERC20Capped{
    // The cap is set in the constructor, fixed permanently at contract
    // deployment.
    constructor(uint256 cap) ERC20("DevToken", "DVT") ERC20Capped(cap){
    }
    // issueToken may be called to mint more (as reward, purchase, whatever)
    // until the cap is reached.
    function issueToken() public{
        _mint(msg.sender,1000*10**18);
    }
}