// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract DevToken is ERC20{
    // In constructor, only mint at the initial smart contract deploy,
    // send full token supply to the address that deployed.
    constructor() ERC20("DevToken", "DVT"){
        _mint(msg.sender,1000*10**18);
    }
}